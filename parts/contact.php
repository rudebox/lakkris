<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<main>
	
	<?php get_template_part('parts/page', 'header');?>

	<div class="contact padding--both">

		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="col-sm-6 contact__form">
				<?php gravity_form( 1, $display_title = true, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); 
				?>
				</div>

				<?php 
					$title = get_field('contact_title');
					$text = get_field('contact_text');
				 ?>

				<div class="col-sm-5 col-sm-offset-1 contact__info blue--bg">					
					<h3 class="contact__title"><?php echo esc_html($title); ?></h3>
					<?php echo $text; ?>					
				</div>
			</div>
		</div>
	</div>
	

</main>

<?php get_template_part('parts/footer'); ?>
